# Terraria v1.4.2 Cheat Table

## Features

- Damage Prevention (Drowning, Burning, Physical)
- Inventory Stack Size Modification
- Set Luck
- Set Placement Range
- Angler Achievement helper
- Player-Pointer finder

## Remarks

### Placement Range

- The placement range is only set if the set range is greater than the current range.

### Angler Achievement

- Note that the achievement function often isn't loaded by default. Viewing the achievements in the game menu might help in some cases.
- It is not recommended to lock the "Quests-Todo"-Record
- By being in-game and setting the "Quests-Todo"-Record will increase Steam's Progress indicator by the set amount. The set value should the jump back to zero.

### Player-Pointer Finder

- The position of the local player structure is located at `PLAYER_POINTER`
- This can be used in the "Structure Dissector" to edit more properties!

### Other

- I am by no means a CheatEngine / Assembly / etc. expert. These files are provided as-is. I will not be liable for any damages done.
- Licence: Public Domain
